import { Component } from "react";
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Row } from "react-bootstrap";

class LuckyDraw extends Component {
    constructor(props) {
        super(props);

        this.state = {
            numberDraw: [],
        }
    }

    handleRandom = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max-min) + min);
    }

    buttonGeneralNumberRandom = () => {
        let numberDraw = []
        for (let i = 0; i < 6; i++) {
            let numRandom = this.handleRandom(10, 99)
            numberDraw.push(numRandom)
        }
        this.setState({
            numberDraw: numberDraw
        })
        console.log(this.state.numberDraw)
    }

   
    render() {
        let numberDraw = this.state.numberDraw
        return (
            <>
            <h2 className="text-center pt-4">Lucky Draw</h2>
            <Row xs="auto" className="justify-content-md-center mt-5">
                {numberDraw.map((number, index) => (
                    <div>
                        <Col key={index}>
                            <div style={{padding:'40px', borderRadius:"50%", backgroundColor: 'orange'}}>
                                <h4>{number}</h4>
                            </div>
                        </Col>
                    </div>
                ))}
            </Row>
            <Row>
            <Col className="text-center">
                <Button onClick={this.buttonGeneralNumberRandom} key="general" className="mt-5 p-3" variant="success">GENERAL</Button>{' '}
            </Col>
        </Row>
        </>
        )
    }
}

export default LuckyDraw;
